package br.com.gilmar.agenda;

import android.widget.EditText;
import android.widget.RatingBar;

import br.com.gilmar.agenda.modelo.Aluno;

/**
 * Created by gilmar on 23/01/16.
 */
public class FormularioHelper {

    private final EditText campoNome;
    private final EditText campoEndereco;
    private final EditText campoEmail;
    private final EditText campoSite;
    private final RatingBar campoNota;
    private Aluno aluno;

    public FormularioHelper(FormularioActivity formularioActivity){
        campoNome = (EditText) formularioActivity.findViewById(R.id.form_nome);
        campoEndereco = (EditText) formularioActivity.findViewById(R.id.form_endereco);
        campoEmail = (EditText) formularioActivity.findViewById(R.id.form_email);
        campoSite = (EditText) formularioActivity.findViewById(R.id.form_site);
        campoNota = (RatingBar) formularioActivity.findViewById(R.id.form_nota);
        aluno = new Aluno();
    }

    public Aluno getAluno() {
        aluno.setNome(campoNome.getText().toString());
        aluno.setEndereco(campoEndereco.getText().toString());
        aluno.setSite(campoSite.getText().toString());
        aluno.setEmail(campoEmail.getText().toString());
        aluno.setNota(Double.valueOf(campoNota.getProgress()));
        return aluno;
    }

    public void preencheForm(Aluno aluno) {
        campoNome.setText(aluno.getNome());
        campoEndereco.setText(aluno.getEndereco());
        campoEmail.setText(aluno.getEmail());
        campoSite.setText(aluno.getSite());
        campoNota.setProgress(aluno.getNota().intValue());
        this.aluno = aluno;
    }
}
