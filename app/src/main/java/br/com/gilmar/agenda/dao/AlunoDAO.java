package br.com.gilmar.agenda.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.gilmar.agenda.modelo.Aluno;

/**
 * Created by gilmar on 23/01/16.
 */
public class AlunoDAO extends SQLiteOpenHelper {


    public AlunoDAO(Context context) {
        super(context, "Agenda", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE ALUNOS (id INTEGER PRIMARY KEY," +
                "                          nome TEXT NOT NULL," +
                "                          endereco TEXT," +
                "                          site TEXT," +
                "                          email TEXT," +
                "                          nota REAL" +
                "                          );";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS ALUNOS;";
        db.execSQL(sql);
        onCreate(db);
    }

    public void insert(Aluno aluno) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = getContentValuesAluno(aluno);

        db.insert("Alunos", null, values);
    }

    @NonNull
    private ContentValues getContentValuesAluno(Aluno aluno) {
        ContentValues values = new ContentValues();
        values.put("nome", aluno.getNome());
        values.put("endereco", aluno.getEndereco());
        values.put("site", aluno.getSite());
        values.put("email", aluno.getEmail());
        values.put("nota", aluno.getNota());
        return values;
    }

    public List<Aluno> getAlunos() {
        List<Aluno> list = new ArrayList<>();
        String sql = "SELECT * FROM ALUNOS;";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()){
            Aluno aluno = new Aluno();
            aluno.setId(c.getLong(c.getColumnIndex("id")));
            aluno.setNome(c.getString(c.getColumnIndex("nome")));
            aluno.setEmail(c.getString(c.getColumnIndex("email")));
            aluno.setSite(c.getString(c.getColumnIndex("site")));
            aluno.setEndereco(c.getString(c.getColumnIndex("endereco")));
            aluno.setNota(c.getDouble(c.getColumnIndex("nota")));

            list.add(aluno);
        }
        c.close();
        return list;
    }

    public void delete(Aluno aluno) {
        SQLiteDatabase db = getWritableDatabase();
        String[] param = {aluno.getId().toString()};
        db.delete("Alunos", "id = ?", param);
    }

    public void update(Aluno aluno) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValuesAluno = getContentValuesAluno(aluno);
        String[] param = {aluno.getId().toString()};
        db.update("Alunos", contentValuesAluno, "id = ?", param);
    }
}
